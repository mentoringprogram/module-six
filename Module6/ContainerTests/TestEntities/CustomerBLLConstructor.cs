﻿using ContainerTests.TestEntities.Interfaces;
using IoC.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting.Logging;

namespace ContainerTests.TestEntities
{
    [ImportConstructor]
    public class CustomerBLLConstructor
    {
        public CustomerBLLConstructor(ICustomerDAL dal, Logger logger) { }
    }
}
