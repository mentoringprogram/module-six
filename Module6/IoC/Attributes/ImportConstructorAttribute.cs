﻿using System;

namespace IoC.Attributes
{
    /// <summary>
    /// Custom class for importing constructors
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ImportConstructorAttribute : Attribute
    {

    }
}
