﻿using System;

namespace IoC.Attributes
{
    /// <summary>
    /// Custom attribute for marking classes
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ExportAttribute : Attribute
    {
        public Type Type { get; private set; }

        public ExportAttribute()
        {

        }

        public ExportAttribute(Type type)
        {
            Type = type;
        }
    }
}
