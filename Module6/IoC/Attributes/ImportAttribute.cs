﻿using System;

namespace IoC.Attributes
{
    /// <summary>
    /// Custom class for importing fields or properties
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class ImportAttribute : Attribute
    {
        
    }
}
