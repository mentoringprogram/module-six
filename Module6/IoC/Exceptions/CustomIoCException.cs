﻿using System;

namespace IoC.Exceptions
{
    public class CustomIoCException : Exception
    {
        public CustomIoCException() : base()
        { }

        public CustomIoCException(string message) : base(message)
        { }
    }
}
