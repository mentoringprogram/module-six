﻿using IoC.Attributes;
using IoC.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace IoC
{
    public class Container
    {
        /// <summary>
        /// Dictionary for registration of types
        /// </summary>
        private readonly Dictionary<Type, Type> typesDictionary;

        public Container()
        {
            typesDictionary = new Dictionary<Type, Type>();
        }

        /// <summary>
        /// Explicit way of adding dependencies
        /// </summary>
        /// <param name="type">Class</param>
        public void AddType(Type type)
        {
            typesDictionary.Add(type, type);
        }

        /// <summary>
        /// Explicit way of adding dependencies 
        /// </summary>
        /// <param name="baseType">Interface</param>
        /// <param name="type">Class</param>
        public void AddType(Type baseType, Type type)
        {
            typesDictionary.Add(baseType, type);
        }

        /// <summary>
        /// Read dependencies from assembly by attributes
        /// </summary>
        /// <param name="assembly">Current assembly</param>
        public void AddAssembly(Assembly assembly)
        {
            //Get all types
            var listOfTypes = assembly.GetTypes();
            foreach (var type in listOfTypes)
            {
                //Get custom attributes
                var typeImportConstrAttr = type.GetCustomAttribute<ImportConstructorAttribute>();
                var typeImportPropAttr = type.GetProperties().Where(x => x.GetCustomAttribute<ImportAttribute>() != null);

                //Check import attributes
                if (typeImportConstrAttr != null || typeImportPropAttr.Any())
                {
                    typesDictionary.Add(type, type);
                }

                //Check export attributes
                var typeExportAttr = type.GetCustomAttributes<ExportAttribute>();
                foreach(var exportAttr in typeExportAttr)
                {
                    if (exportAttr.Type != null)
                    {
                        typesDictionary.Add(exportAttr.Type, type);
                    }
                    else
                    {
                        typesDictionary.Add(type, type);
                    }
                }
            }
        }

        /// <summary>
        /// Get an instance of a class that was previously registered with all dependencies
        /// </summary>
        /// <param name="type">Type of instance</param>
        /// <returns>Instance</returns>
        public object CreateInstance(Type type)
        {
            return CreateInstanceWithDependencies(type);
        }

        public T CreateInstance<T>()
        {
            return (T)CreateInstanceWithDependencies(typeof(T));
        }

        #region Private

        /// <summary>
        /// Recursive method for creating instance
        /// </summary>
        /// <param name="type">Type of instance</param>
        /// <returns>Instance</returns>
        private object CreateInstanceWithDependencies(Type type)
        {
            if (!typesDictionary.ContainsKey(type))
            {
                throw new CustomIoCException($"CreateInstance method thrown an exception. Type {type.Name} is not registered.");
            }

            var typeToGetInstance = typesDictionary[type];
            var constructorOfType = typeToGetInstance.GetConstructors();

            var correctConstructor = constructorOfType.First(); 
            var instance = ResolveConstructor(typeToGetInstance, correctConstructor);
            if (type.GetCustomAttribute<ImportConstructorAttribute>() != null)
            {
                return instance;
            }

            ResolveProperties(type, instance);
            return instance;
        }

        private void ResolveProperties(Type type, object instance)
        {
            var propertiesToResolve = type.GetProperties().Where(x => x.GetCustomAttribute<ImportAttribute>() != null);
            foreach (var property in propertiesToResolve)
            {
                var resolvedProp = CreateInstanceWithDependencies(property.PropertyType);
                property.SetValue(instance, resolvedProp);
            }
        }

        private object ResolveConstructor(Type type, ConstructorInfo constructorInfo)
        {
            var correctConstructorParams = constructorInfo.GetParameters();
            var resolvedParams = new List<object>();
            Array.ForEach(correctConstructorParams, x => resolvedParams.Add(CreateInstanceWithDependencies(x.ParameterType)));
            var instance = Activator.CreateInstance(type, resolvedParams.ToArray());
            return instance;
        }

        #endregion
    }
}
